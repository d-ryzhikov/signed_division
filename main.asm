LOCALS @@
.286
.model small
.stack 100h
.data
    msg_input_dividend db "Please, enter the dividend:", 0Ah, 0Dh, '$'
    msg_input_divider db 0Ah, 0Dh, "Please, enter the divider:", 0Ah, 0Dh, '$'
    msg_invalid_input db 0Ah,0Dh, "Invalid input. Please, try again:", 0Ah, 0Dh, '$'
    msg_quotient db 0Ah,0Dh, "Quotient: ", '$'
    msg_remainder db 0Ah,0Dh, "Remainder: ", '$'
    oct_prefix db "0", '$'
    hex_prefix db "0x", '$'
    new_line db 0Ah,0Dh, '$'
    eq_sign db " = ", '$'

    buf_dividend db 8, 9 dup (0)
    buf_divider db 8, 9 dup (0)

    num_buf db 8 dup (?)
    num_chars db "0123456789ABCDEF"
.code
main:
    mov ax,@data
    mov ds,ax
    mov es,ax

    cld

    lea dx, msg_input_dividend
    call print_string
get_dividend:
    lea dx, buf_dividend
    call get_string
    lea si,buf_dividend+2
    call str_to_num
    pushf
    pop dx
    test dx,100010000000b
    jnz invalid_dividend

    push ax;save result

    lea dx, msg_input_divider
    call print_string
get_divider:
    lea dx, buf_divider
    call get_string
    lea si,buf_divider+2
    call str_to_num
    pushf
    pop dx
    test dx,100010000000b
    jnz invalid_divider
    test ax,ax
    jz invalid_divider;divider can't be zero

    pop bx;get dividend
    xchg ax,bx;ax=dividend, bx=divider
    cmp ax,0
    jl negative
    xor dx,dx
    jmp divide
negative:
    mov dx,0FFFFh

divide:
    idiv bx
    push dx

    lea dx,new_line
    call print_string

    lea dx,msg_quotient
    call print_string

    call print_notations

    lea dx,msg_remainder
    call print_string

    pop ax
    call print_notations

    lea dx,new_line
    call print_string

    mov ax,4C00h
    int 21h

invalid_dividend:
    pushf
    pop dx
    and dx,1111011101111111b
    push dx
    popf
    lea dx,msg_invalid_input
    call print_string
    lea di,buf_dividend+1
    xor ah,ah
    call clear_buf
    jmp get_dividend

invalid_divider:
    pushf
    pop dx
    and dx,1111011101111111b
    push dx
    popf
    lea dx,msg_invalid_input
    call print_string
    lea di,buf_divider+1
    xor ah,ah
    call clear_buf
    jmp get_divider

get_string proc
;ds:dx=buffer address
    push ax
    push dx

    mov ah,0Ah
    int 21h

    pop dx
    pop ax
    ret
endp get_string

print_string proc
;ds:dx=string address
    push ax

    mov ah,09h
    int 21h

    pop ax
    ret
endp print_string

str_to_num proc
;es:si=string to process
;out: ax=result
    mov cx,1

@@skip_spaces:
    lodsb
    cmp al,' '
    je @@skip_spaces
    cmp al,9;tab character
    je @@skip_spaces

    cmp al,'-'
    je @@negative
@@notation:
    cmp al,0Dh;empty string
    je @@empty_string
    cmp al,'0'
    je @@oct_or_hex
@@dec:
    dec si
    call dec_str_to_num
    jmp @@exit

@@oct_or_hex:
    lodsb
    cmp al,'x'
    je @@hex
    cmp al,'X'
    je @@hex
@@oct:
    dec si
    call oct_str_to_num
    jmp @@exit
@@hex:
    call hex_str_to_num
    jmp @@exit

@@negative:
    neg cx
    lodsb
    jmp @@notation

@@empty_string:
    pushf
    pop dx
    or dx,100010000001b;set CF,SF,OF
    push dx
    popf
    jmp @@exit

@@exit:
    ret
endp str_to_num

oct_str_to_num proc
;es:si=string address
;cx=1 for positive number, -1 for negative

;out: ax=result

    xor ax,ax
    xor bx,bx
@@next_char:
    lodsb
    cmp al,0Dh
    je @@get_sign
    cmp al,'0'
    jb @@invalid_string
    cmp al,'7'
    ja @@invalid_string
    sub al,'0'
    xchg ax,bx
    mov dx,8
    imul dx
    jc @@overflow
    add ax,bx
    jo @@overflow
    xchg ax,bx
    jmp @@next_char

@@overflow:
    jmp @@exit
@@invalid_string:
    pushf
    pop dx
    or dx,100010000001b;set CF,SF,OF
    push dx
    popf
    jmp @@exit

@@get_sign:
    mov ax,bx
    imul cx
@@exit:
    ret
endp oct_str_to_num

dec_str_to_num proc
;es:si=string address
;cx=1 for positive number, -1 for negative

;out: ax=result

    xor ax,ax
    xor bx,bx
@@next_char:
    lodsb
    cmp al,0Dh
    je @@get_sign
    cmp al,'0'
    jb @@invalid_string
    cmp al,'9'
    ja @@invalid_string
    sub al,'0'
    xchg ax,bx
    mov dx,10
    imul dx
    jo @@overflow
    add ax,bx
    jo @@overflow
    xchg ax,bx
    jmp @@next_char

@@overflow:
    jmp @@exit
@@invalid_string:
    pushf
    pop dx
    or dx,100010000001b;set CF,SF,OF
    push dx
    popf
    jmp @@exit

@@get_sign:
    mov ax,bx
    imul cx
@@exit:
    ret
endp dec_str_to_num

hex_str_to_num proc
;es:si=string address
;cx=1 for positive number, -1 for negative

;out: ax=result

    xor ax,ax
    xor bx,bx
@@next_char:
    lodsb
    cmp al,0Dh
    je @@get_sign
    cmp al,'0'
    jb @@invalid_string
    cmp al,'f'
    ja @@invalid_string
    cmp al,'9'
    ja @@letter
    sub al,'0'
@@char_converted:
    xchg ax,bx
    mov dx,16
    imul dx
    jo @@overflow
    add ax,bx
    jo @@overflow
    xchg ax,bx
    jmp @@next_char

@@letter:
    cmp al,'A'
    jb @@invalid_string
    cmp al,'a'
    jae @@lowercase
@@uppercase:
    add al,32;make lowercase
@@lowercase:
    cmp al,'a'
    je @@a
    cmp al,'b'
    je @@b
    cmp al,'c'
    je @@c
    cmp al,'d'
    je @@d
    cmp al,'e'
    je @@e
    cmp al,'f'
    je @@f
    jmp @@invalid_string

    @@a:
        mov bl,10
        jmp @@char_converted
    @@b:
        mov bl,11
        jmp @@char_converted
    @@c:
        mov bl,12
        jmp @@char_converted
    @@d:
        mov bl,13
        jmp @@char_converted
    @@e:
        mov bl,14
        jmp @@char_converted
    @@f:
        mov bl,15
        jmp @@char_converted
@@overflow:
    jmp @@exit
@@invalid_string:
    pushf
    pop dx
    or dx,100010000001b;set CF,SF,OF
    push dx
    popf
    pop ax
    jmp @@exit

@@get_sign:
    mov ax,bx
    imul cx
@@exit:
    ret
endp hex_str_to_num

print_int proc
;ax=num to print
;bx=base
    push ax
    push cx
    push di
    push si

    xor si,si
    xor cx,cx
    cmp ax,0
    jge @@positive
    push ax
    mov ah,02h
    mov dl,'-'
    int 21h
    pop ax
    neg ax

@@positive:
    lea di,num_buf
@@next_num:
    xor dx,dx
    div bx
    mov si,dx
    push ax
    mov al,[num_chars+si]
    stosb
    pop ax
    inc cx
    test ax,ax
    jnz @@next_num

    cmp bl,8
    je @@oct
    cmp bl,10
    je @@print
    cmp bl,10h
    je @@hex

@@oct:
    lea dx,oct_prefix
    call print_string
    jmp @@print
@@hex:
    lea dx,hex_prefix
    call print_string

@@print:
    mov ah,02h
@@print_cycle:
    dec di
    mov dl,[di]
    int 21h
    dec cx
    jnz @@print_cycle

@@end:
    pop si
    pop di
    pop cx
    pop ax
    ret
endp print_int

print_notations proc
;ax=number to print

    mov bx,8
    call print_int

    lea dx,eq_sign
    call print_string

    mov bx,10
    call print_int

    lea dx,eq_sign
    call print_string

    mov bx,10h
    call print_int

    ret
endp print_notations

clear_buf proc
;es:di=address of buffer
;ah=filler
next_byte:
    lodsb
    cmp al,ah
    je @@exit
    mov al,ah
    stosb
    jmp next_byte
@@exit:
    ret
endp clear_buf

end main