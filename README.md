#Signed division

Program gets operands in octal, decimal and hexadecimal notations.

Output format: `octal number = decimal number = hexadecimal number`

